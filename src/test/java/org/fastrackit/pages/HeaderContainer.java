package org.fastrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class HeaderContainer {
    private final SelenideElement burgerButton = $("#react-burger-menu-btn");
    private final SelenideElement burgerMenu = $(".bm-menu-wrap");
    private final SelenideElement logoutOption = $("#logout_sidebar_link");
    private final SelenideElement appLogo = $(".app_logo");
    private final SelenideElement peekLogo = $(".peek");
    private final SelenideElement shoppingCartIcon = $("#shopping_cart_container");
    private final SelenideElement productsPageSecondHeader = $(".header_secondary_container .title");
    private final SelenideElement shoppingCartBadge = $(".shopping_cart_badge");
    private final SelenideElement sortMode = $(".product_sort_container");
    private final SelenideElement highToLow = $("option[value=hilo]");

    /**
     * Page Content
     */

    public void clickBurgerButton() {
        burgerButton.click();
    }

    public void clickLogout() {
        logoutOption.click();
    }

    public void clickCartIcon() {
        shoppingCartIcon.click();
    }

    public String productsPageHeader() {
        return productsPageSecondHeader.getText();
    }

    public String productsInCartBadge() {
        return shoppingCartBadge.getText();
    }

    public void clickSortMode() {
        sortMode.click();
    }

    public void clickSortHighToLow() {
        highToLow.click();
    }

    /**
     * Page Verifiers
     */

    public boolean isBurgerMenuDisplayed() {
        return burgerMenu.exists() && burgerButton.isDisplayed();
    }

    public boolean isLogoutOptionDisplayed() {
        return logoutOption.exists() && logoutOption.isDisplayed();
    }

    public boolean isApplicationLogoDisplayed() {
        return appLogo.exists() && appLogo.isDisplayed();
    }

    public boolean isPeekLogoDisplayed() {
        return peekLogo.exists() && peekLogo.isDisplayed();
    }

    public boolean isCartIconDisplayed() {
        return shoppingCartIcon.exists() && shoppingCartIcon.isDisplayed();
    }

    public boolean sortModeIsDisplayed() {
        return sortMode.exists() && sortMode.isDisplayed();
    }

    public boolean sortHighToLowIsDisplayed() {
        return highToLow.exists() && highToLow.isDisplayed();
    }
}
