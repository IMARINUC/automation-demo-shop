package org.fastrackit.pages;

import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.$;

public class Footer {
    private final SelenideElement twitterLogo = $("li.social_twitter > a");
    private final SelenideElement facebookLogo = $("li.social_facebook > a");
    private final SelenideElement linkedinLogo = $("li.social_linkedin > a");
    private final SelenideElement footerRobotLogo = $(".footer_robot");
    private final SelenideElement footerTermsCopy = $(".footer_copy");

    /**
     * Page Content
     */

    public String twitterLogoRedirectURL() {
        return twitterLogo.getAttribute("href");
    }

    public String facebookLogoRedirectURL() {
        return facebookLogo.getAttribute("href");
    }

    public String linkedinLogoRedirectURL() {
        return linkedinLogo.getAttribute("href");
    }

    /**
     * Page Verifiers
     */

    public boolean twitterLogoIsDisplayed() {
        return twitterLogo.exists() && twitterLogo.isDisplayed();
    }

    public boolean facebookLogoIsDisplayed() {
        return facebookLogo.exists() && facebookLogo.isDisplayed();
    }

    public boolean linkedinLogoIsDisplayed() {
        return linkedinLogo.exists() && linkedinLogo.isDisplayed();
    }

    public boolean footerRobotLogoIsDisplayed() {
        return footerRobotLogo.exists() && facebookLogo.isDisplayed();
    }

    public boolean footerTermsCopyDisplayed() {
        return footerTermsCopy.exists() && footerTermsCopy.isDisplayed();
    }
}