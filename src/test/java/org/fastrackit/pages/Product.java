package org.fastrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static java.lang.String.format;

public class Product {
    private final SelenideElement title;
    private final SelenideElement price;

    /**
     * Page Content
     */

    private final String productId;
    private final String expectedPrice;

    public Product(String productId, String price) {
        this.productId = productId;
        this.title = $(format("#item_%s_title_link", productId));
        this.expectedPrice = price;
        this.price = title.parent().parent().$(".pricebar .inventory_item_price");
    }

    public String getProductId() {
        return productId;
    }

    public String getExpectedPrice() {
        return this.expectedPrice;
    }

    public String getPrice() {
        return this.price.getText();
    }

    /**
     * Page Verifiers
     */

    public boolean isDisplayed() {
        return title.exists() && title.isDisplayed();
    }
}
