package org.fastrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class LoginForm {
    private final SelenideElement userName = $("#user-name");
    private final SelenideElement password = $("#password");
    private final SelenideElement loginButton = $("#login-button");
    private final SelenideElement errorMessage = $(".error-message-container [data-test=error]");

    /**
     * Page Content
     */

    public void credentialsLogin(String user, String inputPassword) {
        userName.sendKeys(user);
        password.sendKeys(inputPassword);
        loginButton.click();
    }

    public void clearLoginField() {
        userName.clear();
        password.clear();
    }

    public String errorMessage() {
        return errorMessage.getText();
    }

    /**
     * Page Verifiers
     */

    public boolean isErrorMessageDisplayed() {
        return errorMessage.exists() && errorMessage.isDisplayed();
    }
}
