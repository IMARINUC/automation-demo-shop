package org.fastrackit.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import java.util.Random;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class ProductsPage {
    private final SelenideElement productContainer = $(".inventory_list");
    private final SelenideElement productItem = $(".inventory_item_name");
    private final ElementsCollection addToCartList = $$(".pricebar > button");
    private final ElementsCollection allProducts = $$(".inventory_item_name");
    private final SelenideElement firstProduct = allProducts.first();
    private final ElementsCollection allProductsAfterSort = $$(".inventory_item_name");
    private final SelenideElement firstProductAfterSort = allProductsAfterSort.first();
    private final SelenideElement removeProductItem = $(".btn.btn_secondary.btn_small.btn_inventory");

    /**
     * Page Content
     */

    public void clickOnRandomItemInList() {
        Random rnd = new Random();
        int i = rnd.nextInt(addToCartList.size());
        addToCartList.get(i).click();
    }

    public void addRandomProductToBasket(int productsToAdd) {
        assert productItem.isDisplayed();
        Selenide.sleep(10*300);
        for(int i=1; i<=productsToAdd; i++) {
            clickOnRandomItemInList();
        }
    }

    public void clickRemoveProduct() {
        removeProductItem.click();
    }

    public String firstProductTitleBeforeSort() {
        return firstProduct.getText();
    }

    public String firstProductTitleAfterSort() {
        return firstProductAfterSort.getText();
    }

    /**
     * Page Verifiers
     */


    public boolean isProductContainerDisplayed() {
        return productContainer.exists() && productContainer.isDisplayed();
    }

}
