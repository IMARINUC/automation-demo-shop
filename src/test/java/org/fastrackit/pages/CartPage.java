package org.fastrackit.pages;

import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.$;

public class CartPage {
    private final SelenideElement yourCartSecondHeader = $(".header_secondary_container");
    private final SelenideElement continueShoppingButton = $("#continue-shopping");
    private final SelenideElement checkoutButton = $("#checkout");
    private final SelenideElement checkoutInformationForm = $(".checkout_info");
    private final SelenideElement firstName = $("#first-name");
    private final SelenideElement lastName = $("#last-name");
    private final SelenideElement postalCode = $("#postal-code");
    private final SelenideElement continueButton = $("#continue");
    private final SelenideElement checkoutErrorMessage = $(".error-message-container [data-test=error]");

    /**
     * Page Content
     */

    public String cartHeaderTitle() {
        return yourCartSecondHeader.getText();
    }

    public String firstNameErrorMessage() {
        return checkoutErrorMessage.getText();
    }

    public void clickContinueShoppingButton() {
        continueShoppingButton.click();
    }

    public void clickCheckoutButton() {
        checkoutButton.click();
    }

    public void clickContinueButton() {
        continueButton.click();
    }

    public void checkoutInformation(String inputFirstName, String inputLastName, String inputPostalCode) {
        firstName.sendKeys(inputFirstName);
        lastName.sendKeys(inputLastName);
        postalCode.sendKeys(inputPostalCode);

    }


    /**
     * Page Verifiers
     */

    public boolean isContinueShoppingButtonDisplayed() {
        return continueShoppingButton.exists() && continueShoppingButton.isDisplayed();
    }

    public boolean isCheckoutButtonDisplayed() {
        return checkoutButton.exists() && checkoutButton.isDisplayed();
    }

    public boolean isCheckoutInformationFormDisplayed() {
        return checkoutInformationForm.exists() && checkoutInformationForm.isDisplayed();
    }

    public boolean isFirstNameErrorMessageDisplayed() {
        return checkoutErrorMessage.exists() && checkoutErrorMessage.isDisplayed();
    }
}
