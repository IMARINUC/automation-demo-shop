package org.fastrackit;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.fastrackit.pages.HeaderContainer;
import org.fastrackit.pages.LoginForm;
import org.fastrackit.pages.Product;
import org.testng.Assert;
import org.testng.annotations.*;

import static org.fastrackit.DataSource.*;

public class ProductTest {
    LoginForm loginForm;
    HeaderContainer headerContainer;

    @BeforeClass
    public void openPage() {
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true).savePageSource(true));
        Selenide.open(HOME_PAGE);
        loginForm = new LoginForm();
        headerContainer = new HeaderContainer();
    }
    @BeforeMethod
    public void setLoginForm() {
        loginForm.credentialsLogin(STANDARD_USER, PASSWORD);
    }

    @AfterMethod
    public void logout() {
        headerContainer.clickBurgerButton();
        headerContainer.clickLogout();
    }

    @Test(dataProvider = "getProductData", dataProviderClass = DataSource.class)
    public void verify_products_exist_on_homepage(Product product) {
        Assert.assertTrue(product.isDisplayed(), "Product" + product.getProductId() + "is displayed on page");
    }

    @Test(dataProvider = "getProductData", dataProviderClass = DataSource.class)
    public void verify_products_price_on_homepage(Product product) {
        Assert.assertEquals(product.getPrice(), product.getExpectedPrice(), "Product" + product.getProductId() + "price is" + product.getExpectedPrice());
    }
}

