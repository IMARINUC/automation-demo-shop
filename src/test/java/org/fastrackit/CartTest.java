package org.fastrackit;

import com.codeborne.selenide.Selenide;
import org.fastrackit.pages.CartPage;
import org.fastrackit.pages.HeaderContainer;
import org.fastrackit.pages.LoginForm;
import org.testng.Assert;
import org.testng.annotations.*;

import static org.fastrackit.DataSource.*;

public class CartTest {
    CartPage cartPage;
    LoginForm loginForm;
    HeaderContainer headerContainer;

    @BeforeTest
    public void openPage() {
        Selenide.open(HOME_PAGE);
        loginForm = new LoginForm();
        headerContainer = new HeaderContainer();
        cartPage = new CartPage();
    }
    @BeforeMethod
    public void setLoginForm() {
        loginForm.credentialsLogin(STANDARD_USER, PASSWORD);
    }

    @AfterMethod
    public void logout() {
        headerContainer.clickBurgerButton();
        headerContainer.clickLogout();
    }

    @Test
    public void verify_continue_shopping_button_redirects_to_homepage() {
        headerContainer.clickCartIcon();
        Assert.assertTrue(cartPage.isContinueShoppingButtonDisplayed(), "Continue Shopping Button must be displayed");
        cartPage.clickContinueShoppingButton();
        Assert.assertEquals(headerContainer.productsPageHeader(), "PRODUCTS", "After clicking on the continue shopping button, Products page header is displayed");
    }

    @Test
    public void verify_checkout_button_redirects_to_checkout_info() {
        headerContainer.clickCartIcon();
        Assert.assertTrue(cartPage.isCheckoutButtonDisplayed(), "Checkout Button must be displayed");
        cartPage.clickCheckoutButton();
        Assert.assertTrue(cartPage.isCheckoutInformationFormDisplayed(), "After clicking on the checkout button, the checkout information form is displayed");
    }

    @Test
    public void verify_checkout_information_error_message() {
        headerContainer.clickCartIcon();
        cartPage.clickCheckoutButton();
        cartPage.clickContinueButton();
        Assert.assertTrue(cartPage.isFirstNameErrorMessageDisplayed(), "Error message must be displayed");
        Assert.assertEquals(cartPage.firstNameErrorMessage(), CHECKOUT_INFORMATION_FIRST_NAME_ERROR_MESSAGE, "First name is required error message is displayed");
    }

    @Test
    public void verify_continue_button_redirects_to_checkout_overview() {
        headerContainer.clickCartIcon();
        cartPage.clickCheckoutButton();
        cartPage.checkoutInformation("value1", "value2", "value3");
        cartPage.clickContinueButton();
        Assert.assertEquals(cartPage.cartHeaderTitle(), "CHECKOUT: OVERVIEW", "After clicking on the cart icon, Checkout Page is opened");
    }
    }
