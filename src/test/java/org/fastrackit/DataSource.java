package org.fastrackit;

import org.fastrackit.pages.Product;
import org.testng.annotations.DataProvider;

public class DataSource {
    public static final String HOME_PAGE = "https://www.saucedemo.com/";
    public static final String STANDARD_USER = "standard_user";
    public static final String LOCKED_USER = "locked_out_user";
    public static final String PASSWORD = "secret_sauce";
    public static final String LOCKED_ERROR = "Epic sadface: Sorry, this user has been locked out.";
    public static final String INVALID_CREDENTIALS_ERROR = "Epic sadface: Username and password do not match any user in this service";
    public static final String CHECKOUT_INFORMATION_FIRST_NAME_ERROR_MESSAGE = "Error: First Name is required";

    public static final String SAUCE_LABS_BACKPACK_PRODUCT_ID = "4";
    public static final String SAUCE_LABS_BIKE_LIGHT_PRODUCT_ID = "0";
    public static final String SAUCE_LABS_BOLT_TSHIRT_PRODUCT_ID = "1";
    public static final String SAUCE_LABS_FLEECE_JACKET_PRODUCT_ID = "5";
    public static final String SAUCE_LABS_ONESIE_PRODUCT_ID = "2";
    public static final String TEST_ALL_THE_THINGS_TSHIRT_RED_PRODUCT_ID = "3";

    public static final Product SAUCE_LABS_BACKPACK_PRODUCT = new Product(SAUCE_LABS_BACKPACK_PRODUCT_ID, "$29.99");
    public static final Product SAUCE_LABS_BIKE_LIGHT_PRODUCT = new Product(SAUCE_LABS_BIKE_LIGHT_PRODUCT_ID, "$9.99");
    public static final Product SAUCE_LABS_BOLT_TSHIRT_PRODUCT = new Product(SAUCE_LABS_BOLT_TSHIRT_PRODUCT_ID, "$15.99");
    public static final Product SAUCE_LABS_FLEECE_JACKET_PRODUCT = new Product(SAUCE_LABS_FLEECE_JACKET_PRODUCT_ID, "$49.99");
    public static final Product SAUCE_LABS_ONESIE_PRODUCT = new Product(SAUCE_LABS_ONESIE_PRODUCT_ID, "$7.99");
    public static final Product TEST_ALL_THE_THINGS_TSHIRT_RED_PRODUCT = new Product(TEST_ALL_THE_THINGS_TSHIRT_RED_PRODUCT_ID, "$15.99");

    @DataProvider(name = "getProductData")
    public static Object[][] getProductData() {
        Object[][] products = new Object[6][];
        products[0] = new Object[]{SAUCE_LABS_BIKE_LIGHT_PRODUCT};
        products[1] = new Object[]{SAUCE_LABS_BOLT_TSHIRT_PRODUCT};
        products[2] = new Object[]{SAUCE_LABS_ONESIE_PRODUCT};
        products[3] = new Object[]{TEST_ALL_THE_THINGS_TSHIRT_RED_PRODUCT};
        products[4] = new Object[]{SAUCE_LABS_BACKPACK_PRODUCT};
        products[5] = new Object[]{SAUCE_LABS_FLEECE_JACKET_PRODUCT};
        return products;
    }
}
