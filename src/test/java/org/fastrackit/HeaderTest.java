package org.fastrackit;

import com.codeborne.selenide.Selenide;
import org.fastrackit.pages.*;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.fastrackit.DataSource.*;

public class HeaderTest {
    LoginForm loginForm;
    HeaderContainer headerContainer;
    CartPage cartPage;
    ProductsPage productsPage;

    @BeforeTest
    public void openPage() {
        Selenide.open(HOME_PAGE);
        loginForm = new LoginForm();
        headerContainer = new HeaderContainer();
        cartPage = new CartPage();
        productsPage = new ProductsPage();
    }

    @BeforeMethod
    public void setLoginForm() {
        loginForm.credentialsLogin(STANDARD_USER, PASSWORD);
    }

    @AfterMethod
    public void logout() {
        headerContainer.clickBurgerButton();
        headerContainer.clickLogout();
    }

    @Test(priority = 1)
    public void verify_application_logo() {
        Assert.assertTrue(headerContainer.isApplicationLogoDisplayed(), "Application logo is displayed");
        Assert.assertTrue(headerContainer.isPeekLogoDisplayed(), "Peek logo is displayed");
    }

    @Test(priority = 2)
    public void verify_cart_icon_functionality() {
        Assert.assertTrue(headerContainer.isCartIconDisplayed(), "Cart icon is displayed");
        headerContainer.clickCartIcon();
        Assert.assertEquals(cartPage.cartHeaderTitle(), "YOUR CART", "After clicking on the cart icon, Cart Page is opened");
    }

    @Test(priority = 3)
    public void cart_icon_updates_upon_adding_a_product_to_cart() {
        productsPage.addRandomProductToBasket(1);
        Assert.assertEquals(headerContainer.productsInCartBadge(), "1", "After adding a product to the basket, the cart icon is updated");
        productsPage.clickRemoveProduct();
    }

    @Test(priority = 4)
    public void cart_icon_updates_upon_adding_more_products_to_cart() {
        productsPage.addRandomProductToBasket(2);
        Assert.assertEquals(headerContainer.productsInCartBadge(), "2", "After adding more than one product to the basket, the cart icon is updated");
    }

    @Test(priority = 5)
    public void verify_sort_high_to_low_mode() {
        Assert.assertEquals(productsPage.firstProductTitleBeforeSort(), "Sauce Labs Backpack", "Title is displayed is displayed");
        Assert.assertTrue(headerContainer.sortModeIsDisplayed(), "Sort mode is displayed");
        headerContainer.clickSortMode();
        Assert.assertTrue(headerContainer.sortHighToLowIsDisplayed(), "Sort high to low mode is displayed");
        headerContainer.clickSortHighToLow();
        Assert.assertTrue(productsPage.firstProductTitleBeforeSort() != productsPage.firstProductTitleAfterSort());
    }
}
