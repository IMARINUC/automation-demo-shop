package org.fastrackit;

import com.codeborne.selenide.Selenide;
import org.fastrackit.pages.*;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import static org.fastrackit.DataSource.*;

public class LoginTest {
    LoginForm loginForm;
    HeaderContainer headerContainer;
    CartPage cartPage;
    ProductsPage productsPage;

    @BeforeTest
    public void openPage() {
        Selenide.open(HOME_PAGE);
        loginForm = new LoginForm();
        headerContainer = new HeaderContainer();
        cartPage = new CartPage();
        productsPage = new ProductsPage();
    }

    @Test(priority = 1)
    public void standard_user_login_test() {
        loginForm.credentialsLogin(STANDARD_USER, PASSWORD);
        Assert.assertTrue(productsPage.isProductContainerDisplayed(), "Product page is displayed after successful login");
        headerContainer.clickBurgerButton();
        headerContainer.clickLogout();
    }

    @Test(priority = 2)
    public void verify_logout_functionality() {
        loginForm.credentialsLogin(STANDARD_USER, PASSWORD);
        headerContainer.clickBurgerButton();
        Assert.assertTrue(headerContainer.isBurgerMenuDisplayed(), "After clicking on the burger button, burger menu must be displayed");
        Selenide.sleep(10*100);
        Assert.assertTrue(headerContainer.isLogoutOptionDisplayed(), "Logout sidebar option exists");
        Selenide.sleep(10*100);
        headerContainer.clickLogout();
        Selenide.sleep(10*100);
    }

    @Test(priority = 3)
    public void verify_locked_credentials_show_error_message() {
        loginForm.credentialsLogin(LOCKED_USER, PASSWORD);
        Assert.assertTrue(loginForm.isErrorMessageDisplayed(), "Locked account error message must be displayed");
        Assert.assertEquals(loginForm.errorMessage(), LOCKED_ERROR, "Locked account error message is displayed");
        loginForm.clearLoginField();
    }

    @Test(priority = 4)
    public void verify_invalid_credentials_show_error_message() {
        loginForm.credentialsLogin("user", PASSWORD);
        Assert.assertTrue(loginForm.isErrorMessageDisplayed(), "Invalid credentials error message must be displayed");
        Assert.assertEquals(loginForm.errorMessage(), INVALID_CREDENTIALS_ERROR, "Invalid credentials error message is displayed");
        loginForm.clearLoginField();
    }
}
