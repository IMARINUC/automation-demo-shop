package org.fastrackit;

import com.codeborne.selenide.Selenide;
import org.fastrackit.pages.Footer;
import org.fastrackit.pages.HeaderContainer;
import org.fastrackit.pages.LoginForm;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.fastrackit.DataSource.*;

public class FooterTest {
    LoginForm loginForm;
    Footer footer;
    HeaderContainer headerContainer;

    @BeforeTest
    public void openPage() {
        Selenide.open(HOME_PAGE);
        loginForm = new LoginForm();
        footer = new Footer();
        headerContainer = new HeaderContainer();
    }
    @BeforeMethod
    public void setLoginForm() {
        loginForm.credentialsLogin(STANDARD_USER, PASSWORD);
    }

    @AfterMethod
    public void logout() {
        headerContainer.clickBurgerButton();
        headerContainer.clickLogout();
    }

    @Test
    public void verify_twitter_logo_redirects_to_twitter_page() {
        Assert.assertTrue(footer.twitterLogoIsDisplayed(), "Twitter logo must be displayed");
        Assert.assertEquals(footer.twitterLogoRedirectURL(), "https://twitter.com/saucelabs",
                "Clicking the twitter logo, redirects to SauceLab Twitter page");
    }

    @Test
    public void verify_facebook_logo_redirects_to_facebook_page() {
        Assert.assertTrue(footer.facebookLogoIsDisplayed(), "Facebook logo must be displayed");
        Assert.assertEquals(footer.facebookLogoRedirectURL(), "https://www.facebook.com/saucelabs",
                "Clicking the facebook logo, redirects to SauceLab Facebook page");
    }

    @Test
    public void verify_linkedin_logo_redirects_to_linkedin_page() {
        Assert.assertTrue(footer.linkedinLogoIsDisplayed(), "Linkedin logo must be displayed");
        Assert.assertEquals(footer.linkedinLogoRedirectURL(), "https://www.linkedin.com/company/sauce-labs/",
                "Clicking the linkedin logo, redirects to SauceLab Linkedin page");
    }
    @Test
    public void verify_robot_logo_is_displayed() {
        Assert.assertTrue(footer.footerRobotLogoIsDisplayed(), "Footer robot logo must be displayed");
    }

    @Test
    public void verify_terms_of_services_is_displayed() {
        Assert.assertTrue(footer.footerTermsCopyDisplayed(), "Terms of Services must be displayed");
    }
}
