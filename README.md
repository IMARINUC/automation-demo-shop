# Swag Labs Demo Shop Automation
(Brief presentation for DemoShop Automation - https://www.saucedemo.com/)

- This is the final project within the FastTrackIT Test Automation course with the purpose of testing the main functionality and interface of the SwagLab web application
  - Author: _Irina Marinuc_


- **Tech stack used**:
  - Java17
  - IntelliJ IDEA 2021.3 
  - Apache Maven 
  - Selenide Framework
  - TestNG
  - CSS Selectors
  - Google Chrome, 100.0.4896.75
  - Allure
  - PageObject Models
  - GitLab


- **How to run the tests**
  - git clone https://gitlab.com/IMARINUC/automation-demo-shop.git
  - mvn clean test
  - mvn allure:report
  - mvn allure:serve


- **Page Objects**
  - LoginForm
  - HeaderContainer
  - ProductsPage
  - Product
  - Footer
  - CartPage
  - DataSource


- **Tests implemented**


| TestClass   | Test Name                                              | Test Description                                                                                                                       | Test Status |
|-------------|--------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------|-------------|
| LoginTest   | standard_user_login_test                               | Product page is displayed after login in with the standard credeantials                                                                | **PASSED**|
| LoginTest   | verify_logout_functionality                            | Logout option within the side bar exists and logout is performed after clicking the logout button                                      | **PASSED**|
| LoginTest   | verify_locked_credentials_show_error_message           | Error message for locked credeantials is displayed on the login form when entering the locked credentials                              | **PASSED**|
| LoginTest   | verify_invalid_credentials_show_error_message          | Error message for invalid credeantials is displayed on the login form when entering invalid user name                                  | **PASSED**|
| HeaderTest  | verify_application_logo                                | Swag Labs application logo exists and is displayed on the Header                                                                       | **PASSED**|
| HeaderTest  | verify_cart_icon_functionality                         | Cart icon exists / is displayed and opens the Cart page after cliking on it                                                            | **PASSED**|
| HeaderTest  | verify_cart_icon_updates_upon_adding_a_product_to_cart | After adding a random product to the basket, the cart icon is updated                                                                  | **PASSED**|
| HeaderTest  | verify_cart_icon_updates_upon_adding_more_products_to_cart | After adding more than one random product to the basket, the cart icon is updated                                                      | **PASSED**|
| HeaderTest  | verify_sort_high_to_low_mode                           | Sort high to low mode is displayed. First product before sorting high to low is not the same product after sorting                     | **PASSED**|
| FooterTest  | verify_twitter_logo_redirects_to_twitter_page          | Twitter logo exists and is displayed on the Footer. Aplication's Twitter page is opened after cliking on the logo                      | **PASSED**|
| FooterTest  | verify_facebook_logo_redirects_to_facebook_page        | Facebook logo exists and is displayed on the Footer. Aplication's Facebook page is opened after cliking on the logo                    | **PASSED**|
| FooterTest  | verify_linkedin_logo_redirects_to_linkedin_page        | Linkedin logo exists and is displayed on the Footer. Application's Linkedin page is opened after cliking on the logo                   | **PASSED**|
| FooterTest  | verify_robot_logo_is_displayed                         | Swag robot logo exists and is displayed on the Footer                                                                                  | **PASSED**|
| FooterTest  | verify_terms_of_services_is_displayed                  | Terms of Service exists and is displayed on the Footer                                                                                 | **PASSED**|
| ProductTest | verify_products_exist_on_homepage                      | Verified that all 6 products are displayed on the products page, using DataProvider from DataSource Class                              | **PASSED**|
| ProductTest | verify_products_price_on_homepage                      | Verified that all 6 products have price listed on products page, using DataProvider from DataSource Class                              | **PASSED**|
| CartTest    | verify_continue_shopping_button_redirects_to_homepage  | Within the Cart Page, clicking on the "continue shopping" button will redirect to products page                                        | **PASSED**|
| CartTest    | verify_checkout_button_redirects_to_checkout_info      | Within the Cart Page, clicking on the "Checkout" button will redirect to checkout information form                                     | **PASSED**|
| CartTest    | verify_checkout_information_error_message              | Within the Checkout Information Page, clicking on the "Continue" button will display an error message if no checkout information exist | **PASSED**|
| CartTest    | verify_continue_button_redirects_to_checkout_overview  | "Continue" button redirects to "Checkout Overview Page" after completing the checkout Information form                                 | **PASSED**|


- Reporting is also available in the "allure-results" folder
  - Tests are executed using Maven
  